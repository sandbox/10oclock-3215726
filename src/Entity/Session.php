<?php

namespace Drupal\census\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the page view entity.
 *
 * @ContentEntityType(
 *   id = "census_session",
 *   label = @Translation("Session"),
 *   handlers = {
 *     "storage" = "Drupal\census\SessionStorage",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "census_session",
 *   entity_keys = {
 *     "id" = "sid",
 *     "label" = "visitor",
 *   },
 * )
 */
class Session extends ContentEntityBase implements SessionInterface {

  /**
   * {@inheritdoc}
   */
  public function getVisitor() {
    return $this->get('visitor')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVisitor(string $hash) {
    $this->set('visitor', $hash);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntryPath() {
    return $this->get('entry')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntryPath(string $path) {
    $this->set('entry', $path);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExitPath() {
    return $this->get('exit')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExitPath(string $path) {
    $this->set('exit', $path);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlatform() {
    return $this->get('platform')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlatform(string $platform) {
    $this->set('platform', $platform);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBrowser() {
    return $this->get('browser')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBrowser(string $browser) {
    $this->set('browser', $browser);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserAgent() {
    return $this->get('user_agent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserAgent(string $user_agent) {
    $this->set('user_agent', $user_agent);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getScreenWidth() {
    return $this->get('width')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setScreenWidth(int $width) {
    $this->set('width', $width);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getScreenHeight() {
    return $this->get('height')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setScreenHeight(int $height) {
    $this->set('height', $height);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStartTime() {
    return $this->get('start')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartTime(int $time) {
    $this->set('start', $time);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndTime() {
    return $this->get('end')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndTime(int $time) {
    $this->set('end', $time);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['visitor'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Visitor'))
      ->setRequired(TRUE);

    $fields['entry'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Entry page'));

    $fields['exit'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Exit page'));

    $fields['platform'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Platform'));

    $fields['browser'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Browser'));

    $fields['user_agent'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('User Agent'));

    $fields['width'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Width'))
      ->setSettings([
        'unsigned' => TRUE,
        'size' => 'small',
      ]);

    $fields['height'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Height'))
      ->setSettings([
        'unsigned' => TRUE,
        'size' => 'small',
      ]);

    $fields['start'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Start time'));

    $fields['end'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('End time'));

    return $fields;
  }

}

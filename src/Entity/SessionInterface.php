<?php

namespace Drupal\census\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining session entities.
 */
interface SessionInterface extends ContentEntityInterface {

  /**
   * Gets the visitor hash.
   *
   * @return string
   *   The visitor hash.
   */
  public function getVisitor();

  /**
   * Sets the visitor hash.
   *
   * @param string $hash
   *   The visitor hash.
   *
   * @return $this
   */
  public function setVisitor(string $hash);

  /**
   * Gets the entry path of the session.
   *
   * @return string
   *   The entry path.
   */
  public function getEntryPath();

  /**
   * Sets the entry path of the session.
   *
   * @param string $path
   *   The entry path of the session.
   *
   * @return $this
   */
  public function setEntryPath(string $path);

  /**
   * Gets the exit path of the session.
   *
   * @return string
   *   The exit path.
   */
  public function getExitPath();

  /**
   * Sets the exit path of the session.
   *
   * @param string $path
   *   The exit path of the session.
   *
   * @return $this
   */
  public function setExitPath(string $path);

  /**
   * Gets the visitor's platform.
   *
   * @return string
   *   The platform.
   */
  public function getPlatform();

  /**
   * Sets the visitor's platform.
   *
   * @param string $platform
   *   The visitor's platform.
   *
   * @return $this
   */
  public function setPlatform(string $platform);

  /**
   * Gets the visitor's browser.
   *
   * @return string
   *   The browser name.
   */
  public function getBrowser();

  /**
   * Sets the visitor's browser.
   *
   * @param string $browser
   *   The visitor's browser name.
   *
   * @return $this
   */
  public function setBrowser(string $browser);

  /**
   * Gets the visitor's browser user agent.
   *
   * @return string
   *   The visitor's browser user agent.
   */
  public function getUserAgent();

  /**
   * Sets the visitor's browser user agent.
   *
   * @param string $user_agent
   *   The visitor's browser user agent.
   *
   * @return $this
   */
  public function setUserAgent(string $user_agent);

  /**
   * Gets the visitor's screen width.
   *
   * @return int
   *   The visitor's screen width.
   */
  public function getScreenWidth();

  /**
   * Sets the visitor's screen width.
   *
   * @param int $width
   *   The visitor's screen width.
   *
   * @return $this
   */
  public function setScreenWidth(int $width);

  /**
   * Gets the visitor's screen height.
   *
   * @return int
   *   The visitor's screen height.
   */
  public function getScreenHeight();

  /**
   * Sets the visitor's screen height.
   *
   * @param int $height
   *   The visitor's screen height.
   *
   * @return $this
   */
  public function setScreenHeight(int $height);

  /**
   * Gets the session start time.
   *
   * @return int
   *   The session start time.
   */
  public function getStartTime();

  /**
   * Sets the session start time.
   *
   * @param int $time
   *   The session start time.
   *
   * @return $this
   */
  public function setStartTime(int $time);

  /**
   * Gets the session end time.
   *
   * @return int
   *   The session end time.
   */
  public function getEndTime();

  /**
   * Sets the session end time.
   *
   * @param int $time
   *   The session end time.
   *
   * @return $this
   */
  public function setEndTime(int $time);

}

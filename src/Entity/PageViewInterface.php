<?php

namespace Drupal\census\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining page view entities.
 */
interface PageViewInterface extends ContentEntityInterface {

  /**
   * Gets the session entity.
   *
   * @return \Drupal\census\Entity\SessionInterface
   *   The session entity.
   */
  public function getSession();

  /**
   * Sets the session entity.
   *
   * @param \Drupal\census\Entity\SessionInterface $session
   *   The session entity.
   *
   * @return $this
   */
  public function setSession(SessionInterface $session);

  /**
   * Gets the path of the visited page.
   *
   * @return string
   *   The path.
   */
  public function getPath();

  /**
   * Sets the path of the visited page.
   *
   * @param string $path
   *   The path of the visited page.
   *
   * @return $this
   */
  public function setPath(string $path);

  /**
   * Gets the referrer of the visited page.
   *
   * @return string
   *   The referrer.
   */
  public function getReferrer();

  /**
   * Sets the referrer of the visited page.
   *
   * @param string $referrer
   *   The referrer of the visited page.
   *
   * @return $this
   */
  public function setReferrer(string $referrer);

}

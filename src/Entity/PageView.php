<?php

namespace Drupal\census\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the page view entity.
 *
 * @ContentEntityType(
 *   id = "census_page_view",
 *   label = @Translation("Page view"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "census_page_view",
 *   entity_keys = {
 *     "id" = "pvid",
 *     "label" = "path",
 *   },
 * )
 */
class PageView extends ContentEntityBase implements PageViewInterface {

  /**
   * {@inheritdoc}
   */
  public function getSession() {
    return $this->get('session')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setSession(SessionInterface $session) {
    $this->set('session', $session->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->get('path')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPath(string $path) {
    $this->set('path', $path);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferrer() {
    return $this->get('referrer')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setReferrer(string $referrer) {
    $this->set('referrer', $referrer);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['session'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Session'))
      ->setSetting('target_type', 'census_session')
      ->setRequired(TRUE);

    $fields['path'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Path'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['referrer'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Referrer'));

    return $fields;
  }

}

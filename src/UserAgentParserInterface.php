<?php

namespace Drupal\census;

/**
 * Interface for defining a user agent parser.
 */
interface UserAgentParserInterface {

  /**
   * Set the user agent.
   *
   * @param string $user_agent
   *   The user agent string.
   *
   * @return $this
   */
  public function setUserAgent(string $user_agent);

  /**
   * Get the platform.
   *
   * @return string
   *   The platform name.
   */
  public function getPlatform();

  /**
   * Get the browser.
   *
   * @return string
   *   The browser name.
   */
  public function getBrowser();

}

<?php

namespace Drupal\census;

/**
 * Interface for defining an analytics service.
 */
interface AnalyticsInterface {

  /**
   * Create an anonymized visitor hash.
   *
   * @param string $ip
   *   The visitor's IP address.
   * @param string $host
   *   The host name of the visited site.
   * @param string $user_agent
   *   The visitor's user agent.
   *
   * @return string
   *   The anonymized visitor hash.
   */
  public function getVisitorHash(string $ip, string $host, string $user_agent);

}

<?php

namespace Drupal\census;

use DeviceDetector\DeviceDetector;

/**
 * Service for parsing user agent strings.
 */
class UserAgentParser implements UserAgentParserInterface {

  /**
   * The user agent parser.
   *
   * @var \DeviceDetector\DeviceDetector
   */
  protected $parser;

  /**
   * The user agent.
   *
   * @var string
   */
  protected $userAgent;

  /**
   * Set the user agent parser.
   *
   * @param \DeviceDetector\DeviceDetector $parser
   *   The user agent parser.
   *
   * @return $this
   */
  public function setParser(DeviceDetector $parser) {
    $this->parser = $parser;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserAgent(string $user_agent) {
    $this->parser->setUserAgent($user_agent);
    $this->parser->parse();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlatform() {
    return $this->parser->getOs('name');
  }

  /**
   * {@inheritdoc}
   */
  public function getBrowser() {
    return $this->parser->getClient('name');
  }

}

<?php

namespace Drupal\census\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\census\AnalyticsInterface;
use Drupal\census\UserAgentParserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for page view routes.
 */
class PageViewController extends ControllerBase {

  /**
   * The analytics service.
   *
   * @var \Drupal\census\AnalyticsInterface
   */
  protected $analytics;

  /**
   * The user agent parser service.
   *
   * @var \Drupal\census\UserAgentParserInterface
   */
  protected $parser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = (new static())
      ->setAnalytics($container->get('census.analytics'))
      ->setUserAgentParser($container->get('census.user_agent_parser'))
      ->setTime($container->get('datetime.time'));

    return $instance;
  }

  /**
   * Set the analytics service.
   *
   * @param \Drupal\census\AnalyticsInterface $analytics
   *   The analytics service.
   *
   * @return $this
   */
  protected function setAnalytics(AnalyticsInterface $analytics) {
    $this->analytics = $analytics;
    return $this;
  }

  /**
   * Set the user agent parser service.
   *
   * @param \Drupal\census\UserAgentParserInterface $parser
   *   The user agent parser service.
   *
   * @return $this
   */
  protected function setUserAgentParser(UserAgentParserInterface $parser) {
    $this->parser = $parser;
    return $this;
  }

  /**
   * Set the time service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   *
   * @return $this
   */
  protected function setTime(TimeInterface $time) {
    $this->time = $time;
    return $this;
  }

  /**
   * Insert a page view record.
   */
  public function insert(Request $request) {
    $data = Json::decode($request->getContent());

    $ip = $request->getClientIp();
    $host = $request->getHost();
    $visitor_hash = $this->analytics->getVisitorHash($ip, $host, $data['userAgent']);

    /** @var \Drupal\census\SessionStorageInterface $session_storage */
    $session_storage = $this->entityTypeManager()->getStorage('census_session');
    $session = $session_storage->loadByHash($visitor_hash);

    if (!$session) {
      $this->parser->setUserAgent($data['userAgent']);

      /** @var \Drupal\census\Entity\SessionInterface $session */
      $session = $session_storage->create([
        'visitor' => $visitor_hash,
        'entry' => $data['path'],
        'platform' => $this->parser->getPlatform(),
        'browser' => $this->parser->getBrowser(),
        'user_agent' => $data['userAgent'],
        'width' => $data['width'],
        'height' => $data['height'],
        'start' => $this->time->getRequestTime(),
      ]);

      $violations = $session->validate();
      if ($violations->count() > 0) {
        $errors = [];

        foreach ($violations as $violation) {
          $errors[] = $this->t('@path: @message', [
            '@path' => $violation->getPropertyPath(),
            '@message' => $violation->getMessage()->__toString(),
          ]);
        }

        return new JsonResponse($errors, 422);
      }

      $session->save();
    }

    $page_view_storage = $this->entityTypeManager()->getStorage('census_page_view');
    /** @var \Drupal\census\Entity\PageViewInterface $page_view */
    $page_view = $page_view_storage->create([
      'session' => $session,
      'path' => $data['path'],
      'referrer' => $data['referrer'],
    ]);

    $violations = $page_view->validate();
    if ($violations->count() > 0) {
      $errors = [];

      foreach ($violations as $violation) {
        $errors[] = $this->t('@path: @message', [
          '@path' => $violation->getPropertyPath(),
          '@message' => $violation->getMessage()->__toString(),
        ]);
      }

      return new JsonResponse($errors, 422);
    }

    $page_view->save();

    return new JsonResponse();
  }

}

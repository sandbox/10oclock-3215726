<?php

namespace Drupal\census;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\KeyValueStore\KeyValueFactory;

/**
 * Census analytics service.
 */
class Analytics implements AnalyticsInterface {

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs the service.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactory $key_value
   *   The key value store factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(KeyValueFactory $key_value, TimeInterface $time) {
    $this->keyValue = $key_value->get('census');
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getVisitorHash(string $ip, string $host, string $user_agent) {
    return Crypt::hashBase64($this->getSalt() . $ip . $host . $user_agent);
  }

  /**
   * Get a rotating salt.
   *
   * @return string
   *   The current salt.
   */
  protected function getSalt() {
    $this->rotateSalt();
    return $this->keyValue->get('census_salt');
  }

  /**
   * Rotate the salt value.
   */
  protected function rotateSalt() {
    $salt_expiration = $this->keyValue->get('census_salt_expiration', 0);
    $time = $this->time->getRequestTime();

    if ($salt_expiration < $time) {
      $salt = Crypt::randomBytesBase64();
      $this->keyValue->set('census_salt', $salt);

      $tomorrow = (new \DateTime('tomorrow'))->getTimestamp();
      $this->keyValue->set('census_salt_expiration', $tomorrow);
    }
  }

}

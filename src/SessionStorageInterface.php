<?php

namespace Drupal\census;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Interface defining the storage handler for Session entities.
 */
interface SessionStorageInterface extends ContentEntityStorageInterface {

  /**
   * Load a Session entity by visitor hash.
   *
   * @param string $hash
   *   The visitor hash.
   *
   * @return \Drupal\census\Entity\SessionInterface|false
   *   The Session entity, or FALSE if a session was not found.
   */
  public function loadByHash(string $hash);

}

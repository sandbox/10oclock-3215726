<?php

namespace Drupal\census;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage handler for Session entities.
 */
class SessionStorage extends SqlContentEntityStorage {

  /**
   * {@inheritdoc}
   */
  public function loadByHash(string $hash) {
    $sessions = $this->loadByProperties(['visitor' => $hash]);

    if (empty($sessions)) {
      return FALSE;
    }

    return reset($sessions);
  }

}

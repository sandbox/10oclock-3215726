/**
 * @file
 * Webpack config for compiling javascript.
 */

const path = require('path');

module.exports = {
  entry: './js/census.es6.js',
  module: {
    rules: [
      {
        test: /census\.es6\.js/,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env"]]
          }
        }
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'js'),
    filename: 'census.min.js',
  },
};

/**
 * @file
 * Census tracking script.
 */

import fetch from 'unfetch';

((Drupal) => {
  'use strict';

  Drupal.behaviors.census = {
    attach: function() {
      const page = once('census', 'html').shift();

      if (page) {
        fetch('/session/token')
          .then(r => r.text())
          .then(token => {
            insertPageView(token);
          });
      }
    }
  };

  let insertPageView = (token) => {
    let data = {
      'path': window.location.pathname,
      'referrer': document.referrer,
      'platform': navigator.platform,
      'userAgent': navigator.userAgent,
      'width': window.innerWidth,
      'height': window.innerHeight,
    };

    fetch('/census/page-view/insert?_format=json', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': token
      },
      body: JSON.stringify(data)
    });
  }
})(Drupal);
